# frozen_string_literal: true

module MarleySpoon
  class Photo < Base
    attr_accessor :id, :url

    def initialize(args)
      super(args)

      self.url = parse_url(args)
    end

    def self.find(id)
      response = Request.get("assets/#{id}/")
      new(response)
    end

    private

    def parse_url(args)
      url_data = args.dig('fields', 'file', 'url')
      url_data&.gsub(%r{^//}, 'http://')
    end
  end
end
