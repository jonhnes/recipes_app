# frozen_string_literal: true

module MarleySpoon
  class Recipe < Base
    attr_accessor :id,
                  :title,
                  :photo,
                  :calories,
                  :description,
                  :chef,
                  :tags

    BASE_PATH = 'entries'
    CONTENT_TYPE = 'recipe'
    SELECT_FROM_INDEX = 'sys.id,fields.title,fields.photo'

    def self.all
      response = Request.get(BASE_PATH, content_type: CONTENT_TYPE,
                                        select: SELECT_FROM_INDEX)
      items = response.fetch('items', [])
      extras = response.fetch('includes', {})
      recipes = items.map { |recipe| Recipe.new(recipe.merge(extras)) }
      [recipes, response[:errors]]
    end

    def initialize(args = {})
      super(args)

      assets = args.fetch('Asset', {})
      self.photo = parse_photo(photo, assets)
      self.chef = parse_chef(chef)
      self.tags = parse_tags(tags)
    end

    private

    def parse_photo(args, extras)
      return nil if args.blank?

      id = get_id args
      info = get_info_from id, extras
      return Photo.new info if info.present?

      Photo.find get_id(args)
    end

    def parse_chef(args)
      return nil if args.blank?

      Chef.find get_id(args)
    end

    def parse_tags(args)
      return [] if args.blank?

      args.map { |tag| Tag.find get_id(tag) }
    end

    def get_info_from(id, args)
      args.select { |e| e.dig('sys', 'id') == id }.first
    end
  end
end
