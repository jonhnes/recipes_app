# frozen_string_literal: true

module MarleySpoon
  class Chef < Base
    attr_accessor :id, :name

    def initialize(args)
      super(args)
    end
  end
end
