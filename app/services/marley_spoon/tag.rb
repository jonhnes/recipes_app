# frozen_string_literal: true

module MarleySpoon
  class Tag < Base
    attr_accessor :id, :name

    def initialize(args)
      super(args)
    end
  end
end
