# frozen_string_literal: true

module MarleySpoon
  class Base
    attr_accessor :errors

    def self.find(id)
      response = Request.get("entries/#{id}/")
      new(response)
    end

    def initialize(args = {})
      return unless args

      self.id = get_id(args)
      args.fetch('fields', {}).each do |name, value|
        attr_name = name.to_s.underscore
        send("#{attr_name}=", value) if respond_to?("#{attr_name}=")
      end
    end

    def get_id(args)
      args.dig('sys', 'id')
    end
  end
end
