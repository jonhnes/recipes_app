# frozen_string_literal: true

class RecipesController < ApplicationController
  def index
    @recipes, @message = MarleySpoon::Recipe.all
  end

  def show
    @recipe = MarleySpoon::Recipe.find(params[:id])
  end
end
