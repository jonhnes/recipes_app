# frozen_string_literal: true

require 'faraday'
require 'json'

module MarleySpoon
  class Connection
    BASE = 'https://cdn.contentful.com/spaces/'

    def self.api
      Faraday.new(url: url) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
        faraday.headers['Content-Type'] = 'application/json'
      end
    end

    def self.url
      BASE + chave_id
    end

    def self.chave_id
      Rails.application.credentials.marley_spoon[:space_id]
    end
  end
end
