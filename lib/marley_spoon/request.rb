# frozen_string_literal: true

module MarleySpoon
  class Request
    class << self
      def get(id, query = {})
        response, status = get_json(id, query)
        status == 200 ? response : errors(response)
      end

      def errors(response)
        error = { errors: { status: response['status'],
                            message: response['message'] } }
        response.merge(error)
      end

      def get_json(root_path, query = {})
        query.merge! access_token: access_token
        query_string = query.map { |k, v| "#{k}=#{v}" }.join('&')
        path = "#{root_path}?#{query_string}"
        response = api.get path
        [JSON.parse(response.body), response.status]
      end

      def api
        MarleySpoon::Connection.api
      end

      def access_token
        Rails.application.credentials.marley_spoon[:access_token]
      end
    end
  end
end
