require 'rails_helper'

RSpec.describe MarleySpoon::Connection do
  describe '.api' do
    subject { described_class.api }

    it { is_expected.to an_instance_of Faraday::Connection }
    it do
      expect(Faraday).to receive(:new)
        .with(url: match('https://cdn.contentful.com/spaces'))
      subject
    end
    it { expect(subject.headers['Content-Type']).to eq 'application/json' }
  end

  describe '.url' do
    subject { described_class.url }

    it { is_expected.to match(/https:\/\/cdn.contentful.com\/spaces\/.+/) }
  end

  describe '.chave_id' do
    after { described_class.chave_id }

    it do
      expect(Rails).to receive_message_chain(:application, :credentials,
                                             :marley_spoon, :[]).with(:space_id)
    end
  end
end
