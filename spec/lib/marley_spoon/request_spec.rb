require 'rails_helper'

RSpec.describe MarleySpoon::Request do
  describe 'get' do
    let(:id) { rand(1..9) }
    let(:query) { { Faker::Lorem.word => Faker::Lorem.word } }
    let(:response) { {} }
    let(:json) { [response, status] }

    before do
      allow(described_class).to receive(:get_json)
        .with(id, query).and_return(json)
    end

    subject { described_class.get(id, query) }

    context 'when status 200' do
      let(:status) { 200 }

      it { is_expected.to eq response }
    end

    context 'when status 404' do
      let(:status) { 404 }

      it do
        expect(described_class).to receive(:errors).with(response)
        subject
      end
    end
  end

  describe '.errors' do
    let(:status) { [200, 404].sample }
    let(:message) { Faker::Lorem.sentence }
    let(:response) { { 'status' => status, 'message' => message } }

    subject { described_class.errors response }

    it do
      expect(subject[:errors]).to include(status: status, message: message)
    end
  end

  describe '.api' do
    after { described_class.api }

    it do
      expect(MarleySpoon::Connection).to receive(:api)
    end
  end

  describe 'get_json' do
    let(:root_path) { Faker::Internet.url }

    subject { described_class.get_json root_path, query }

    context 'when has query parameter' do
      let(:parameter) { Faker::Lorem.word }
      let(:value) { Faker::Lorem.word }
      let(:query) { { parameter => value } }
      let(:json) { Faker::Json.shallow_json }
      let(:status) { [200, 400].sample }
      let(:response) { double body: json, status: status }

      it do
        expect_any_instance_of(Faraday::Connection).to receive(:get)
          .with(match("#{parameter}=#{value}")).and_return(response)
        is_expected.to include(JSON.parse(json), status)
      end

      it do
        expect_any_instance_of(Faraday::Connection).to receive(:get)
          .with(match('access_token=')).and_return(response)
        is_expected.to include(JSON.parse(json), status)
      end
    end

    context 'when has not query parameter' do
      let(:query) { {} }
      let(:json) { Faker::Json.shallow_json }
      let(:status) { [200, 400].sample }
      let(:response) { double body: json, status: status }

      it do
        expect_any_instance_of(Faraday::Connection).to receive(:get)
          .with(match('access_token=')).and_return(response)
        is_expected.to include(JSON.parse(json), status)
      end
    end
  end

  describe '.access_token' do
    after { described_class.access_token }

    it do
      expect(Rails).to receive_message_chain(:application, :credentials,
                                             :marley_spoon, :[])
        .with(:access_token)
    end
  end
end
