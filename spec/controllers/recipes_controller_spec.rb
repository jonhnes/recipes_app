require 'rails_helper'

RSpec.describe RecipesController, type: :controller do
  describe 'get index' do
    let(:recipe) { MarleySpoon::Recipe.new {} }
    subject { get :index }

    before { allow(MarleySpoon::Recipe).to receive(:all) { [[recipe], nil] } }

    it { is_expected.to have_http_status(:success) }
    it do
      subject
      expect(assigns(:recipes)).to include recipe
    end
  end

  describe 'get show' do
    let(:recipe) { MarleySpoon::Recipe.new {} }

    subject { get :show, params: { id: 'cachorro' } }

    before { allow(MarleySpoon::Recipe).to receive(:find) { recipe } }

    it { is_expected.to have_http_status(:success) }
    it do
      subject
      expect(assigns(:recipe)).to eq recipe
    end
  end
end
