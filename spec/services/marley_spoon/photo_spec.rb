require 'rails_helper'

RSpec.describe MarleySpoon::Photo do
  let(:id) { Faker::Crypto.md5 }
  let(:url) { Faker::Internet.url }
  let(:url_api) { url.gsub(%r{^http://}, '//') }
  let(:args) do
    { 'sys' => { 'id' => id },
      'fields' => { 'file' => { 'url' => url_api } } }
  end
  describe 'initialize' do
    subject { described_class.new(args) }

    it { is_expected.to have_attributes id: id, url: url }
  end

  describe '.find' do
    subject { described_class.find(id) }

    before do
      allow(MarleySpoon::Request).to receive(:get).with("assets/#{id}/")
                                                  .and_return(args)
    end

    it do
      is_expected.to an_instance_of(described_class)
        .and(have_attributes(id: id, url: url))
    end
  end
end
