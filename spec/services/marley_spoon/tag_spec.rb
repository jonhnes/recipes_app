require 'rails_helper'

RSpec.describe MarleySpoon::Tag do
  let(:id) { Faker::Crypto.md5 }
  let(:name) { Faker::Name.name }
  let(:args) { { 'sys' => { 'id' => id }, 'fields' => { 'name' => name } } }

  describe 'initialize' do
    subject { described_class.new(args) }

    it { is_expected.to have_attributes id: id, name: name }
  end

  describe '.find' do
    subject { described_class.find(id) }

    before do
      allow(MarleySpoon::Request).to receive(:get).with("entries/#{id}/")
                                                  .and_return(args)
    end

    it do
      is_expected.to an_instance_of(described_class)
        .and(have_attributes(id: id, name: name))
    end
  end
end
