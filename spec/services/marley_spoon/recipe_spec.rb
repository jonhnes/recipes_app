require 'rails_helper'

RSpec.describe MarleySpoon::Recipe do
  let(:select) { 'sys.id,fields.title,fields.photo' }
  let(:id) { Faker::Crypto.md5 }
  let(:title) { Faker::Lorem.sentence }
  let(:description) { Faker::Lorem.sentence }
  let(:calories) { rand(100..10_000) }
  let(:url) { Faker::Internet.url }
  let(:photo_id) { Faker::Crypto.md5 }
  let(:photo_args) { { 'sys' => { 'id' => photo_id } } }
  let(:chef_id) { Faker::Crypto.md5 }
  let(:chef_args) { { 'sys' => { 'id' => chef_id } } }
  let(:tag_id) { Faker::Crypto.md5 }
  let(:tags_args) { { 'sys' => { 'id' => tag_id } } }
  let(:args) do
    { 'sys' => { 'id' => id },
      'fields' => { 'title' => title, 'photo' => photo_args,
                    'description' => description,  'calories' => calories,
                    'chef' => chef_args, 'tags' => [tags_args] } }
  end

  describe 'initialize' do
    subject { described_class.new args }

    before do
      allow(MarleySpoon::Photo).to receive(:find)
        .with(photo_id).and_return(MarleySpoon::Photo.new(photo_args))
      allow(MarleySpoon::Chef).to receive(:find)
        .with(chef_id).and_return(MarleySpoon::Chef.new(chef_args))
      allow(MarleySpoon::Tag).to receive(:find)
        .with(tag_id).and_return(MarleySpoon::Tag.new(tags_args))
    end

    it do
      is_expected.to have_attributes(
        id: id, title: title, description: description, calories: calories,
        photo: an_instance_of(MarleySpoon::Photo)
               .and(have_attributes(id: photo_id)),
        tags: include(an_instance_of(MarleySpoon::Tag)
              .and(have_attributes(id: tag_id)))
      )
    end
  end

  describe '.all' do
    subject { described_class.all }

    context 'when reponse was success' do
      let(:response) { { 'items' => [args] } }
      before do
        allow(MarleySpoon::Request).to receive(:get)
          .with('entries', content_type: 'recipe', select: select) { response }
        allow(MarleySpoon::Photo).to receive(:find)
          .with(photo_id).and_return(MarleySpoon::Photo.new(photo_args))
        allow(MarleySpoon::Chef).to receive(:find)
          .with(chef_id).and_return(MarleySpoon::Chef.new(chef_args))
        allow(MarleySpoon::Tag).to receive(:find)
          .with(tag_id).and_return(MarleySpoon::Tag.new(tags_args))
      end

      it { is_expected.to include(an_instance_of(Array), nil) }
      it do
        expect(subject.first).to include(
          an_instance_of(MarleySpoon::Recipe).and(
            have_attributes(
              id: id, title: title, description: description,
              calories: calories,
              photo: an_instance_of(MarleySpoon::Photo)
                     .and(have_attributes(id: photo_id)),
              chef: an_instance_of(MarleySpoon::Chef)
                    .and(have_attributes(id: chef_id)),
              tags: include(an_instance_of(MarleySpoon::Tag)
                            .and(have_attributes(id: tag_id)))
            )
          )
        )
      end
    end

    context 'when response has error' do
      let(:message) { Faker::Lorem.sentence }
      let(:response) { { errors: message } }
      before do
        allow(MarleySpoon::Request).to receive(:get)
          .with('entries', content_type: 'recipe', select: select) { response }
      end

      it { is_expected.to include(be_empty, message) }
    end
  end

  describe 'find' do
    let(:id) { Faker::Crypto.md5 }

    subject { described_class.find id }

    before do
      allow(MarleySpoon::Request).to receive(:get)
        .with("entries/#{id}/").and_return(args)
      allow(MarleySpoon::Photo).to receive(:find)
        .with(photo_id).and_return(MarleySpoon::Photo.new(photo_args))
      allow(MarleySpoon::Chef).to receive(:find)
        .with(chef_id).and_return(MarleySpoon::Chef.new(chef_args))
      allow(MarleySpoon::Tag).to receive(:find)
        .with(tag_id).and_return(MarleySpoon::Tag.new(tags_args))
    end
    it do
      is_expected.to an_instance_of(MarleySpoon::Recipe).and(
        have_attributes(
          id: id, title: title, description: description, calories: calories,
          photo: an_instance_of(MarleySpoon::Photo)
                 .and(have_attributes(id: photo_id)),
          chef: an_instance_of(MarleySpoon::Chef)
                 .and(have_attributes(id: chef_id)),
          tags: include(an_instance_of(MarleySpoon::Tag)
                .and(have_attributes(id: tag_id)))
        )
      )
    end
  end
end
