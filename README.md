# Recipes 

Recipes application

## Getting Started

### Prerequisites

To run the project you should have docker and docker-compose installed.

Get master.key attached in the email and copy to the config folder.

After that

```
docker-compose up
```

## Tools
[Heroku](https://recipes-app-2020.herokuapp.com/) for deployment

[Bitbucket](https://bitbucket.org/jonhnes/recipes_app/addon/pipelines/home) Pipelines for CI/CD


## Authors

* **Jonhnes Lopes** - [jonhnes](https://github.com/jonhnes)
